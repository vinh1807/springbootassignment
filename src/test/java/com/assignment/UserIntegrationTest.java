package com.assignment;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.assignment.dao.UserDao;
import com.assignment.entity.User;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=SpringBootAssignmentApplication.class)
public class UserIntegrationTest {
	
	@Autowired
	UserDao userDao;
	
	@Test
	public void testGetUserById() {
		User user = new User("vinh");
		userDao.save(user);
		
		User fromDB = userDao.findById(Integer.valueOf(user.getUserId())).get();
		assertEquals(user.getName(), fromDB.getName());
	}

}
