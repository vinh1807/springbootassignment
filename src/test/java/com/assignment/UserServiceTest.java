package com.assignment;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.assignment.dao.UserDao;
import com.assignment.entity.User;
import com.assignment.service.UserService;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
	
	@InjectMocks
	UserService userService;

	@Mock
	private UserDao userDao;

	@Before
	public void setUp() {
		Optional<User> user = Optional.of(new User("vinh"));

		when(userDao.findById(any(Integer.class))).thenReturn(user);
	}
	
	@Test
	public void whenGetLoyalPoint_ThenReturnLong() {
		long loyalPoint = userService.getLoyalPointByid(1);
		assertEquals(5, loyalPoint);
	}

}
