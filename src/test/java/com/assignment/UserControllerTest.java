package com.assignment;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.mockito.ArgumentMatchers.anyInt;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.assignment.controller.UserController;
import com.assignment.entity.User;
import com.assignment.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {
	
	String jsonObject;
	User user = new User("vinh");
	private ObjectMapper mapper;
	private MockMvc mvc;

	@InjectMocks
	UserController userController;

	@Mock
	private UserService userService;

	@Before
	public void setUp() throws JsonProcessingException {
		
		mapper = new ObjectMapper();

		user.setUserId(1);
		jsonObject = mapper.writeValueAsString(user);

		mvc = MockMvcBuilders.standaloneSetup(userController).build();
		when(userService.findById(anyInt())).thenReturn(user);
	}
	
	@Test
	public void givenUser_whenGetUser_thenReturnJsonUser() throws Exception {
		mvc.perform(get("/user/public/1").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().json(jsonObject));

		verify(userService, times(1)).findById(anyInt());
	}

}
