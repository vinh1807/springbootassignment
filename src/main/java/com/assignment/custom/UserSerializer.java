//package com.assignment.custom;
//
//import java.io.IOException;
//import java.util.Date;
//import java.util.concurrent.TimeUnit;
//
//import com.assignment.entity.User;
//import com.fasterxml.jackson.core.JsonGenerator;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.SerializerProvider;
//import com.fasterxml.jackson.databind.ser.std.StdSerializer;
//
//public class UserSerializer extends StdSerializer<User> {
//	   
//    /**
//	 * 
//	 */
//	private static final long serialVersionUID = -7766673496302919754L;
//
//	public UserSerializer() {
//        this(null);
//    }
//   
//    public UserSerializer(Class<User> t) {
//        super(t);
//    }
// 
//    @Override
//    public void serialize(
//      User value, JsonGenerator jgen, SerializerProvider provider) 
//      throws IOException, JsonProcessingException {
//  
//        jgen.writeStartObject();
//        jgen.writeNumberField("id", value.getUserId());
//        jgen.writeStringField("name", value.getName());
//        jgen.writeStringField("startDate", value.getStartDate().toString());
//        jgen.writeNumberField("loyalPoint",5*(1 + TimeUnit.DAYS.convert(Math.abs((new Date()).getTime() - value.getStartDate().getTime()), TimeUnit.MILLISECONDS)));
//        jgen.writeEndObject();
//    }
//	
//}
