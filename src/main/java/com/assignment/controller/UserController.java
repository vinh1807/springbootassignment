package com.assignment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.entity.User;
import com.assignment.response.Response;
import com.assignment.response.UserNameResponse;
import com.assignment.response.UserStartDateResponse;
import com.assignment.service.UserService;

@RestController
@RequestMapping("user")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@GetMapping(value = "/public/{id}", produces = "application/json; charset=utf-8")
	public User getUserById(@PathVariable("id") int id) {
		return userService.findById(id);
	}

	@GetMapping("/startdate/{id}")
	public UserStartDateResponse getStartDateById(@PathVariable("id") int id) {
		return new UserStartDateResponse(userService.findById(id), Response.SUCCESS, "success");
	}

	@GetMapping("/name/{id}")
	public UserNameResponse getNameById(@PathVariable("id") int id) {
		return new UserNameResponse(userService.findById(id), Response.SUCCESS, "success");
	}

}
