package com.assignment.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.assignment.entity.Account;

public interface AccountDao extends JpaRepository<Account, Integer>  {
	
	boolean existsByUsername(String username);
	
	Account findByUsername(String username);

}
