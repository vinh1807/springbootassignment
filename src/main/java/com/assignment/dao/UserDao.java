package com.assignment.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.assignment.entity.User;

public interface UserDao extends JpaRepository<User, Integer> {

}
