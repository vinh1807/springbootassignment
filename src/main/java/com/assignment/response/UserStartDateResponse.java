package com.assignment.response;

import java.util.Date;

import com.assignment.entity.User;

public class UserStartDateResponse extends Response {
	
	Date startDate;

	public UserStartDateResponse(User user, boolean success, String message) {
		super(success, message);
		this.startDate = user.getStartDate();
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

}
