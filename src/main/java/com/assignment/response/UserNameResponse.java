package com.assignment.response;

import com.assignment.entity.User;

public class UserNameResponse extends Response {
	
	String name;

	public UserNameResponse(User user, boolean success, String message) {
		super(success, message);
		this.name = user.getName();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
