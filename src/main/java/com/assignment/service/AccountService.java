package com.assignment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment.dao.AccountDao;
import com.assignment.entity.Account;

@Service
public class AccountService {

	@Autowired
	AccountDao accountDao;

	public List<Account> findAll() {
		return accountDao.findAll();
	}

	public Account findById(int id) {
		return accountDao.findById(Integer.valueOf(id)).orElse(null);
	}

	public boolean add(Account account) {
		if (accountDao.existsByUsername(account.getUsername()) || account.getAccountId() != 0) {
			return false;
		}

		accountDao.save(account);
		return true;
	}

	public Account findAccountByUsername(String username) {
		return accountDao.findByUsername(username);
	}

	public boolean checkLogin(Account account) {
		if (accountDao.existsByUsername(account.getUsername())) {
			if (accountDao.findByUsername(account.getUsername()).getPassword().equals(account.getPassword())) {
				return true;
			}
		}

		return false;
	}
}
