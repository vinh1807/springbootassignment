package com.assignment.service;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment.dao.UserDao;
import com.assignment.entity.User;

@Service
public class UserService {
	
	@Autowired
	private UserDao userDao;
	
	public User findById(int id) {
		return userDao.findById(Integer.valueOf(id)).get();
	}
	
	public long getLoyalPointByid(int id) {
		User user = userDao.findById(Integer.valueOf(id)).get();
		return 5*(1 + TimeUnit.DAYS.convert(Math.abs((new Date()).getTime() - user.getStartDate().getTime()), TimeUnit.MILLISECONDS));
	}

}
