package com.assignment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.assignment.dao.AccountDao;
import com.assignment.dao.UserDao;
import com.assignment.entity.Account;
import com.assignment.entity.User;

@Component
public class ApplicationDataInitialize implements ApplicationRunner {

	@Autowired
	private UserDao userRepository;

	@Autowired
	private AccountDao accountRepository;

	public void run(ApplicationArguments args) {
		for (int i = 1; i < 10; i++) {
			userRepository.save(new User("lala" + i));
		}
		Account accountAdmin = new Account("admin", "123456", "ROLE_ADMIN");
		Account accountMod = new Account("mod", "654321", "ROLE_MOD");
		accountRepository.save(accountAdmin);
		accountRepository.save(accountMod);
	}

}
